Name: Alex Archer, Aarcher@uoregon.edu

Grader Notes: I use WSL. One of the issues I have is that in docker-compuse the volume command uses ':' as a delimiter. Because I use WSL some of my filestructures are like C:/Users/Name, so the colon causes issues. I was able to remap... SOME things to make it /mnt/c/Users/Name but it was giving me a number of envionrment path errors. 
My solution was to comment out the volume line in the docker-compuse. This may cause inconsistencies between linux and windows linux machines.



This project is a demonstration of using ajax and mongodb on top of the rusa.org brevet time calculator.
Brevet times are calculated according to the algorithm described https://rusa.org/pages/acp-brevet-control-times-calculator

Control times are didicated by the length of the route and a minimum and maximum speed. 

Submit will submit times in the form to the database.
Display will show the submitted times from the database.
Reset will clear the times from the database.

If you attempt to display an empty database an error page will be shown. You can test
this feature by clearing the database with the reset button and observing. 

No other errors are accounted for. 

To run use docker-compose up or docker-docmpuse up --build


